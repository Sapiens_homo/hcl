
#ifndef __HCL_CODEC_BASE64_H__
#define __HCL_CODEC_BASE64_H__

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

	char *BASE64_encode( const char *restrict, const size_t );
	size_t BASE64_decode( const char *restrict, char **restrict );

#ifdef __cplusplus
}
#endif

#endif
