CC = gcc
arch ?= amd64
CFLAGS = -Wall -march=native -std=gnu99
CFLAGS_DBG = $(CFLAGS) -g -D DBG
C_FILES=$(shell find src -name "*.c")
ASM_SRC = $(shell find src -name "*-$(arch).s" )
OBJ = $(patsubst %.c, %.c.o, $(C_FILES) )
OBJ_ASM = $(patsubst %.s, %.s.o, $(ASM_SRC) );
OBJ_DBG = $(patsubst %.c, %.c.o.dbg, $(C_FILES) )
OBJ_ASM_DBG = $(patsubst %.s, %.s.o.dbg, $(ASM_SRC) );
OBJ_FILE = $(shell find . -name "*.o*" )

TARGET = libhcl.so
TARGET_DBG = libhcldbg.so

all: $(TARGET)
debug: $(TARGET_DBG)
clean:
	@echo Cleaning...
	@-rm -f $(OBJ_FILE)
	@-rm $(TARGET)
	@-rm $(TARGET_DBG)
	@echo Done

%.s.o: %.s
	@echo Compiling and generating object $@
	@$(CC) -c -o $@ $^

%.c.o: %.c
	@echo Compiling and generating object $@
	@$(CC) $(CFLAGS) -c -o $@ $^

%.s.o.dbg: %.s
	@echo Compiling and generating debugging object $@
	@$(CC) -g -c -o $@ $^

%.c.o.dbg: %.c
	@echo Compiling and generating debugging object $@
	@$(CC) $(CFLAGS_DBG) -c -o $@ $^

$(TARGET): $(OBJ) $(OBJ_ASM)
	@echo Generating library $@
	@$(CC) -shared -o $@ $^
$(TARGET_DBG): $(OBJ_DBG) $(OBJ_ASM_DBG)
	@echo Generating debugging library $@
	@$(CC) -shared -o $@ $^
