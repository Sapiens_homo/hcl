
#ifndef __HCL_HASH_MD5_H__
#define __HCL_HASH_MD5_H__

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

	char *MD5_hash ( const char *restrict, const size_t );

	int MD5_cmp( const char *restrict, const char *restrict );

#ifdef __cplusplus
}
#endif

#endif
