
#ifndef __HCL_MEM_MEMPOOL_H__
#define __HCL_MEM_MEMPOOL_H__

#include <stdlib.h>

#define __MEMPOOL_BLOCK_SIZE__ 512

#ifdef __cplusplus
extern "C" {
#endif

	struct _MEM_POOL_ {
		size_t size;
		unsigned char align;
		void *block;
		/*
		 * void * next block
		 * void * start address for mapping
		 * void * end address for mapping
		 * ... data [ __MEMPOOL_BLOCK_SIZE__ ]
		 */
	};

	struct _MEM_POOL_ *MEMPOOL_malloc( size_t );
	void MEMPOOL_free( struct _MEM_POOL_ * );
	void *MEMPOOL_get( struct _MEM_POOL_ *, const void *, size_t );
	/* MEMPOOL_set( pool, dst, src, length ) */
	void MEMPOOL_set( struct _MEM_POOL_ *, const void *, const void *, size_t );
	void MEMPOOL_memset( struct _MEM_POOL_ *, const void *, int, size_t );

#ifdef __cplusplus
}
#endif

#endif
