
#include "../../base64.h"

#include <stdint.h>
#include <limits.h>
#include <string.h>

#if CHAR_BIT != 8
#error "UNSUPPORT CHAR_BIT"
#endif

char *BASE64_encode( const char *restrict str, const size_t len ){
	const static char* const __BASE64_MAPPING_ENCODE__ = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	register uint32_t buf;
	register size_t i, ii, iii;

	register char *ret = NULL;
	register size_t pad = ( 3 - ( len % 3 ) ) % 3;
	register size_t rlen = ( ( ( len + 2 ) / 3 ) << 2 ) + 1;

	ret = malloc( rlen );
	memset( ret, 0, rlen );
	
	for ( i = 0, ii = 0; i < len; i += 3, ii += 4 ){
		buf = 0;
		for ( iii = 0; iii < 3; iii++ ){
			buf <<= CHAR_BIT;
			if ( i + iii  < len ){
				buf |= *( str + i + iii );
			}
		}
		iii = 4;
		while ( iii-- ){
			*( ret + ii + iii ) = *( __BASE64_MAPPING_ENCODE__ + ( buf & 0x3F ) );
			buf >>= 6;
		}
	}

	while ( pad-- ){
		*( ret + rlen - 1 - pad ) = '=';
	}

	return ret;
}
