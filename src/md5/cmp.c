
#include "../../md5.h"

int MD5_cmp ( const char *restrict a, const char *restrict b ){
	register int ret = a && b;

	for ( register size_t i = 0; i < 16 && ret; i++ ){
		ret = *( a + i ) == *( b + i ) && ret;
	}

	return ret;
}
