
#include "../../md5.h"
#include <limits.h>
#include <string.h>
#include <stdint.h>

/*
 * LITTLE-ENDIAN
 * SUPPOSE INT is 32bit, CHAR_BIT = 8
 */

#if CHAR_BIT == 8 && __SIZEOF_INT__ == 4

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__

char *MD5_hash ( const char *restrict src, const size_t len ){
	register char *ret = NULL;
	register char *padded = NULL;
	register size_t pad = ( len + sizeof ( len ) + 63 );

	register size_t i, ii;
	register int A, B, C, D;
	register int F, g;

	const static int s[64] = {
		7, 12, 17, 22,	7, 12, 17, 22,	7, 12, 17, 22,	7, 12, 17, 22,
		5,  9, 14, 20,	5,  9, 14, 20,	5,  9, 14, 20,	5,  9, 14, 20,
		4, 11, 16, 23,	4, 11, 16, 23,	4, 11, 16, 23,	4, 11, 16, 23,
		6, 10, 15, 21,	6, 10, 15, 21,	6, 10, 15, 21,	6, 10, 15, 21,
	};

	const static int K[64] = {
		0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
		0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
		0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
		0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
		0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
		0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
		0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
		0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
		0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
		0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
		0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
		0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
		0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
		0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
		0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
		0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391,
	};

	if ( !src ){
		return NULL;
	}

	ret = malloc( 16 );

	pad = ( pad & 0x3f ) ^ pad;
	padded = malloc( pad );

	memset ( padded, 0, pad );
	*( padded + len ) = 0x80;
	memcpy ( padded, src, pad );
	*( (size_t *)padded + pad - sizeof( len ) ) = len & 0xFFFFFFFFFFFFFFFFUL;

	*( ( int* )ret + 0 ) = 0x67452301;
	*( ( int* )ret + 1 ) = 0xefcdab89;
	*( ( int* )ret + 2 ) = 0x98badcfe;
	*( ( int* )ret + 3 ) = 0x10325476;

	for ( i = 0; i < pad; i += 0x40 ){
		A = *( ( int* )ret + 0 );
		B = *( ( int* )ret + 1 );
		C = *( ( int* )ret + 2 );
		D = *( ( int* )ret + 3 );

		for ( ii = 0; ii < 0x40; ii++ ){
			if ( ii < 16 ){
				F = ( B & C ) | ( ( ~B ) & D );
				g = ii;
			} else if ( ii < 32 ){
				F = ( D & B ) | ( ( ~D ) & C );
				g = ( 5 * ii + 1 ) & 0x0f;
			} else if ( ii < 48 ){
				F = B ^ C ^ D;
				g = ( 3 * ii + 5 ) & 0x0f;
			} else {
				F = C ^ ( B | ( ~D ) );
				g = 7 * ii & 0x0f;
			}

			F = F + A + K[ii] + *( ( uint32_t * )( src + i ) + g );
			A = D;
			D = C;
			C = B;
			B = B + ( F << s[ii] | F >> ( CHAR_BIT * sizeof( F ) - s[ii] ) );
		}

		*( ( int* )ret + 0 ) += A;
		*( ( int* )ret + 1 ) += B;
		*( ( int* )ret + 2 ) += C;
		*( ( int* )ret + 3 ) += D;
	}

	free ( padded );

	return ret;
}

#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__

#endif

#elif CHAR_BIT == 8

#else

char *MD5_hash ( const char *restrict str, const size_t len ){
	return NULL;
}

#endif
