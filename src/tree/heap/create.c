
#include "../../../structure/heap.h"
#include <string.h>
#include <stdlib.h>

HCl_heap HCl_heap_create( size_t elem, int (*cmp)(const void *, const void *) ){
	register HCl_heap ret = NULL;

	ret = malloc( sizeof( *ret ) );
	memset( ret, 0, sizeof( *ret ) );
	( *ret ).cmp = cmp;
	( *ret ).elem = elem;
	( *ret ).size = __HCl_HEAP_INCREASE;
	( *ret ).data = malloc( elem * ( *ret ).size );
	memset( ( *ret ).data, 0, elem * ( *ret ).size );

	return ret;
}
