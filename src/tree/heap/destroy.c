
#include "../../../structure/heap.h"
#include <stdlib.h>

void HCl_heap_destroy( HCl_heap heap ){
	free( ( *heap ).data );
	free( heap );
}
