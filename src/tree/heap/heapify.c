
#include "../../../structure/heap.h"
#include <string.h>
#include <stdlib.h>

HCl_heap HCl_heapify( void *restrict data, size_t len, size_t elem, int (*cmp)(const void *, const void *) ){
	register HCl_heap ret = NULL;
	register size_t i;

	ret = malloc( sizeof( *ret ) );
	memset( ret, 0, sizeof( *ret ) );
	( *ret ).cmp = cmp;
	( *ret ).elem = elem;
	( *ret ).len = len;
	( *ret ).size = __HCl_HEAP_ALIGN( len );
	( *ret ).data = malloc( elem * ( *ret ).size );

	memset( ( *ret ).data, 0, elem * ( *ret ).size );
	memcpy( ( *ret ).data, data, elem * len );

	for ( i = 0; i < len; i++ ){
		HCl_sift_( ret, i);
	}

	return ret;
}
