
#include "../../../structure/heap.h"
#include <string.h>
#include <stdlib.h>

HCl_heap HCl_heap_merge( HCl_heap a, HCl_heap b ){
	register size_t i;

	if ( !b ){
		return a;
	}

	if ( !a ){
		a = HCl_heap_create( ( *b ).elem, ( *b ).cmp );
	}

	if ( ( *a ).size < ( *a ).len + ( *b ).len ){
		( *a ).size = __HCl_HEAP_ALIGN( ( *a ).len + ( *b ).len );
	}

	memcpy(
			( *a ).data + ( *a ).elem * ( *a ).len,
			( *b ).data, ( *b ).elem * ( *b ).len
	);

	for ( i = 0; i < ( *a ).len; i++ ){
		HCl_sift_( a, i );
	}

	return a;
}
