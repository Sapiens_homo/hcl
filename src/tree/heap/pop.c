
#include "../../../structure/heap.h"
#include <string.h>
#include <stdlib.h>

void *HCl_heap_pop( HCl_heap heap ){
	register void *ret = NULL;

	if ( !heap || !( *heap ).data || !( *heap ).len  ){
		return NULL;
	}

	ret = malloc( ( *heap ).elem );

	memcpy( ret, ( *heap ).data, ( *heap ).elem );
	memcpy( ( *heap ).data, ( *heap ).data + ( *heap ).elem * ( ( *heap ).len - 1 ), ( *heap ).elem );
	( *heap ).len--;
	HCl_sift_down_( heap, 0 );

	return ret;
}
