
#include "../../../structure/heap.h"
#include <string.h>
#include <stdlib.h>

HCl_heap HCl_heap_push( HCl_heap heap, void *val ){
	if ( !heap ){
		return NULL;
	}

	if ( ( *heap ).len + 1 >= ( *heap ).size ){
		( *heap ).size = __HCl_HEAP_ALIGN( ( *heap ).len + 1 );
		( *heap ).data = realloc( ( *heap ).data, ( *heap ).elem * ( *heap ).size );
	}

	memcpy( ( *heap ).data + ( *heap ).elem * ( *heap ).len, val, ( *heap ).elem );
	HCl_sift_( heap, ( *heap ).len );

	( *heap ).len++;

	return heap;
}
