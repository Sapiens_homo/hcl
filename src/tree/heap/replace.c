
#include "../../../structure/heap.h"
#include <string.h>

HCl_heap HCl_heap_replace( HCl_heap heap, void *val ){
	if ( !heap ){
		return NULL;
	}

	if ( ( *heap ).len ){
		memcpy( ( *heap ).data, val, ( *heap ).elem );
		HCl_sift_down_( heap, 0 );
	}else{
		heap = HCl_heap_push( heap, val );
	}

	return heap;
}
