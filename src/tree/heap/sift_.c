
#include "../../../structure/heap.h"
#include <string.h>
#include <stdlib.h>

void HCl_sift_( HCl_heap heap, size_t i ){
	register void *tmp;
	register void *a;
	register void *b;
	register size_t t;
	if ( !heap ){ return; }
	tmp = alloca( ( *heap ).elem );
	while ( i ){
		t = ( i - 1 ) >> 1;
		if (
			( *heap ).cmp(
				a = ( *heap ).data + ( *heap ).elem * t,
				b = ( *heap ).data + ( *heap ).elem * i
			) >= 0
		){
			break;
		}
		/* swap */
		memcpy( tmp, a, ( *heap ).elem );
		memcpy( a, b, ( *heap ).elem );
		memcpy( b, tmp, ( *heap ).elem );

		i = t;
	}
}
