
#include "../../../structure/heap.h"
#include <string.h>
#include <stdlib.h>

void HCl_sift_down_( HCl_heap heap, size_t i ){
	register size_t l;
	register size_t r;
	register size_t s;
	register void *tmp = alloca( ( *heap ).elem );
	register void *a;
	register void *b;
	while ( ( l = ( i << 1 ) + 1 ) < ( *heap ).len ){
		r = l + 1;
		s = l;
		if (
			r < ( *heap ).len &&
			( *heap ).cmp(
				( *heap ).data + ( *heap ).elem * r,
				( *heap ).data + ( *heap ).elem * l
			) < 0
		){
			s = r;
		}

		if (
			( *heap ).cmp(
				a = ( *heap ).data + ( *heap ).elem * i,
				b = ( *heap ).data + ( *heap ).elem * s
			) <= 0
		){
			break;
		}
		/* swap */
		memcpy( tmp, a, ( *heap ).elem );
		memcpy( a, b, ( *heap ).elem );
		memcpy( b, tmp, ( *heap ).elem );

		i = s;
	}
}
