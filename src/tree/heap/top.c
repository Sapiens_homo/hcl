
#include "../../../structure/heap.h"

void *HCl_heap_top( HCl_heap heap ){
	if ( !heap || ( *heap ).data ){
		return NULL;
	}

	return ( *heap ).data;
}
