
#ifndef __HCl_STRUCTURE_HEAP_H__
#define __HCl_STRUCTURE_HEAP_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

#define __HCl_HEAP_INCREASE (8)

#if __HCl_HEAP_INCREASE == ( __HCl_HEAP_INCREASE & (-(__HCl_HEAP_INCREASE)) )
#define __HCl_HEAP_ALIGN(a) \
	( ( ( (a) + (__HCl_HEAP_INCREASE) - 1 ) >> __builtin_ctz( __HCl_HEAP_INCREASE ) ) \
	  << __builtin_ctz( __HCl_HEAP_INCREASE ) )
#else
#define __HCl_HEAP_ALIGN(a) \
	( ( (a) + (__HCl_HEAP_INCREASE) - 1 ) / __HCl_HEAP_INCREASE * \
	  __HCl_HEAP_INCREASE )
#endif

	struct HCl_Heap {
		size_t len;
		size_t elem;
		size_t size;
		void *data;
		int (*cmp)(const void *, const void *);
	};

	typedef struct HCl_Heap * HCl_heap;

	HCl_heap HCl_heap_create( size_t, int (*)(const void *, const void *) );
	HCl_heap HCl_heapify( void *restrict, size_t, size_t, int (*)(const void *, const void *) );
	HCl_heap HCl_heap_merge( HCl_heap, HCl_heap ); /* Merge second one into first one */
	HCl_heap HCl_heap_push( HCl_heap, void * );
	HCl_heap HCl_heap_replace( HCl_heap, void * );
	void *HCl_heap_pop( HCl_heap );
	void *HCl_heap_top( HCl_heap );

	void HCl_heap_destroy( HCl_heap );

	void HCl_sift_( HCl_heap, size_t );
	void HCl_sift_down_( HCl_heap, size_t );

#ifdef __cplusplus
}
#endif

#endif
