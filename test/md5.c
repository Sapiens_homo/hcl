
#include "../md5.h"
#include <stdio.h>
#include <string.h>

int main( int argc, char **argv ){
	register char *str;
	register char *out;
	register size_t i;
	if ( argc - 1 ){
		str = *( argv + 1 );
	}else{
		str = "Hello, World!";
	}

	out = MD5_hash( str, strlen( str ) );

	printf( "\"%s\": ", str );
	for ( i = 0; i < 16; i++ ){
		printf( "%hhx", *( out + i ) );
	}
	free( out );
}
